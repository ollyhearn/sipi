from django.apps import AppConfig


class QuiztimeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'quiztime'
