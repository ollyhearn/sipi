from django.db import models

# Create your models here.

TEST_TYPES = (('one-choice', 'Choice One'), ('many-choice', 'Choice Many'), ('freeinput', 'Free Input Field'))

class Test(models.Model):
    title = models.TextField(
        verbose_name="Title"
    )
    time_limit = models.IntegerField(
        verbose_name="Time Limit",
        null=True,
        blank=True
    )


class Answer(models.Model):
    answer = models.TextField(
        verbose_name="Answer Contents"
    )


class Question(models.Model):
    question = models.TextField(
        verbose_name="Question Contents"
    )
    type = models.CharField(choices=TEST_TYPES, default='one-choice', max_length=20)
    correct_answers = models.ManyToManyField(
        Answer,
        verbose_name="Correct Answers"
    )

class TestQuestion(models.Model):
    test = models.ForeignKey(
        verbose_name="Test",
        to=Test,
        on_delete=models.CASCADE
    )
    question = models.ForeignKey(
        verbose_name="Question",
        to=Question,
        on_delete=models.CASCADE
    )


class QuestionAnswer(models.Model):
    question = models.ForeignKey(
        verbose_name="Question",
        to=Question,
        on_delete=models.CASCADE
    )
    answer = models.ForeignKey(
        verbose_name="Answer",
        to=Answer,
        on_delete=models.CASCADE
    )

class UserTest(models.Model):
    test = models.ForeignKey(
        verbose_name="Test",
        to=Test,
        on_delete=models.CASCADE
    )
    owner = models.ForeignKey(
        verbose_name="Added to Test User",
        to="auth.User",
        on_delete=models.CASCADE
    )
