from rest_framework import serializers
from quiztime.models import Test, TestQuestion, Question, Answer, QuestionAnswer
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']


class TestSerializer(serializers.ModelSerializer):
    def get_questions(self, obj: Test) -> list:
        return TestQuestion.objects.filter(test=obj)

    class Meta:
        model = Test
        fields = (
            "id",
            "title",
        )
        read_only_fields = ("id",)
        questions = serializers.SerializerMethodField(read_only=True)


class QuestionSerializer(serializers.ModelSerializer):
    def get_answers(self, obj: Question):
        return QuestionAnswer.objects.filter(question=obj)

    class Meta:
        model = Question
        fields = (
            "id",
            "question",
            "type",
        )
        read_only_fields = ("id",)
        answers = serializers.SerializerMethodField(read_only=True)
