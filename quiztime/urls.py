from django.urls import path
from quiztime import views

urlpatterns = [
    path('users/<int:pk>/', views.UserDetail.as_view()),
    path("test/", views.TestList.as_view()),
    path("test/<int:pk>", views.TestDetail.as_view()),
    path("question/", views.QuestionList.as_view()),
    path("question/<int:pk>", views.QuestionDetail.as_view()),

]
